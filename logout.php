<?php
session_start();
session_destroy();
?>

<?php include "include/functions.php"; ?>

<html lang="en">
<head>
    <?php getHeader(); ?>
</head>

<body>
<?php getNavigation(); ?>

<div class="container">

    <hr class="mb-5">
    <div class="row">
        <p>You are logged out. Back to <a href="index.php">
                <button>Homepage</button>
            </a></p>
    </div>
</div>

<?php getFooter(); ?>

</body>
</html>