<?php include "include/functions.php"; ?>

<html lang="en">

<head>

    <?php getHeader(); ?>
</head>

<body>

<?php getNavigation(); ?>

<div class="container">

    <hr class="mb-5">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <hr class="mb-6">
            <hr class="mb-5">
            <h4 class="h2 text-center" style="font-family: 'Lalezar', cursive; color: #401603">Sign in using email or username</h4>
            <hr class="mb-5">
            <form class="form needs-validation" novalidate  data-request="loginUser" data-url="include/api.php" data-method="POST">
                <div class="form-group">
                    <label for="email">E-mail | username</label>
                    <input data-data="emailUsername" type="text" class="form-control" id="emailUsername"
                           placeholder="E-mail | username">
                    <div class="valid-feedback">Valid.</div>
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input data-data="password" type="password" class="form-control" id="password"
                           placeholder="Enter password ">
                    <div class="valid-feedback">Valid.</div>
                    <div class="invalid-feedback">Please fill out this field.</div>
                </div>

                <button type="submit" class="btn btn-primary" id="submitLogin">Login</button>
                <div class="alert alert-primary response d-none" role="alert"></div>

            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>

<?php getFooter(); ?>

</body>

</html>