<?php
//$files = array_diff(scandir(dirname(__DIR__,1)), array('.', '..'));
if (!isset($_SESSION['loggedin']) || !$_SESSION['loggedin']) {
    $files = array("Home", "Sign In", "Sign Up");
} else {
    $files = array("Home", "Sign Out", "Add a New Post");
}
?>
<!--<nav class="navbar navbar-expand-lg navbar-dark bg-dark">-->
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">

    <a class="navbar-brand" href="index.php" style='font-size:58px;font-weight:900;color:#63ff34'>S<i class='fas fa-blog' style='font-size:58px;color:#ffe709'></i>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">

            <?php
            $menuLink = "index.php";
            foreach ($files as $file) {
                if ($file == "Sign In") {
                    $menuLink = "login.php";
                } elseif ($file == "Sign Up") {
                    $menuLink = "register.php";
                } elseif ($file == "Sign Out") {
                    $menuLink = "logout.php";
                } elseif ($file == "Add a New Post") {
                    $menuLink = "newpost.php";
                }
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $menuLink; ?>">
                        <?php echo $file; ?>
                    </a>
                </li>

            <?php } ?>

        </ul>
    </div>
</nav>