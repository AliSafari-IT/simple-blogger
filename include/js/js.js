$(document).ready(function () {
    console.log("initializing parameters for editPost.php from js.js");
    let editTitleTextH2 = $("#editTitleText");

    const bgColor = $('html').css("background-color");
    let  postTitleGroup = $("#newPostTitleGroup");
    postTitleGroup.hide();

    let thisPostID =  $("#postID");

    let newPostTitleInEditor = $("#summernoteTitle");

    let newPostTitleInputBox = $("#newPostTitle");

    let updateDB = $("#updateDatabaseButton");
    updateDB.hide();

    let editTitleButton = $("#editPostTitleButton");

    let previewTitleChange = $("#previewTitleChange");
    previewTitleChange.hide();

    let postContentTextDiv = $("#postContentTextDiv");

    let  postContentGroup = $("#newPostContentGroup");
    postContentGroup.hide();

    let newPostContentInEditor = $("#summernote");
    let newPostContentInputBox = $("#newPostContent");

    let editContentButton = $("#editContentButton");

    let previewPostChange = $("#previewPostChange");
    previewPostChange.hide();

    let editPostButton = $("#editContentButton");

    editTitleButton.click(function () {
        postTitleGroup.show();
        newPostTitleInputBox.css('background-color','yellow');
        newPostTitleInputBox.prop('disabled', true);
        newPostTitleInputBox.hide();
        previewTitleChange.show();
    });

    previewTitleChange.click(function () {
        previewTitleChange.hide();
        const updatedTitle = newPostTitleInEditor.val();
        editTitleTextH2.html(updatedTitle);
        newPostTitleInputBox.val(updatedTitle);

        const updatedPost = newPostContentInEditor.val();
        postContentTextDiv.html(updatedPost);
        newPostContentInputBox.val(updatedPost);

        postTitleGroup.hide();
        updateDB.show();
        console.log("Title updated: "+updatedTitle);
    });

    editContentButton.click(function () {
        postContentGroup.show();
        previewPostChange.show();
        newPostContentInputBox.css('background-color','yellow');
        newPostContentInputBox.prop('disabled', true).hide();

    });

    previewPostChange.click(function () {
        previewPostChange.hide();
        const updatedTitle = newPostTitleInEditor.val();
        editTitleTextH2.html(updatedTitle);
        newPostTitleInputBox.val(updatedTitle);

        const updatedPost = newPostContentInEditor.val();
        postContentTextDiv.html(updatedPost);
        newPostContentInputBox.val(updatedPost);

        postContentGroup.hide();
        updateDB.show();
        console.log("Post content updated: "+updatedPost);

    });

    updateDB.click(function () {
        previewTitleChange.hide();
        previewPostChange.hide();
        updateDB.hide();
    });


    $(document).on('submit', 'form', function (e) {

        e.preventDefault();
        self=this;
        var request = $(this).data("request");
        var url = $(this).data("url");
        var method = $(this).data("method");
        var data = {};
        $.each($('input'), function (key, value) {
            console.log(key + ':' + $(value).val());
        });
        $(this).find('input').each(function( key, value ) {
            if (key == 0 || key == 13 || key==26){
            data[$(value).data("data")] = $(value).val();
            };
        });
        console.log(data);

        $.ajax({
            method: method,
            url: url,
            data: {request: request, data: data },
            dataType: 'JSON'
        }).done(function (jsonData) {

            var data = JSON.parse(jsonData);
            console.log(data);

            if (data['message']) {
                showToast(data['message']);
            }
            if(data['redirect']){
                window.location.href = data['redirect'];
            }

        });

    });

    $(document).on('click','.post .actionbutton',function(){

        self = this;

        var id_post = $(this).parent().data('id_post');

        var request = $(this).data("request");
        var url = $(this).data("url");
        var method = $(this).data("method");
        var deleteElement = $(this).data("deleteElement");
        var data = {};

        data['id'] = id_post;

        $.ajax({method: "POST", url: url, data: {request: request, data: data }}).done(function( jsonData ) {

            var data = JSON.parse(jsonData);

            if (data['message']) {
                showToast(data['message']);
                $(self).parent().parent().remove();
            }
            if(data['redirect']){
                window.location.href = data['redirect'];
            }

        });

    });

});


function generateTable(data){

    var table = "<div class='table-responsive'><table class='table'><thead class='thead-dark'><tr>";

    Object.keys(data[0]).forEach(function(key) {
        table += "<th>";
        table += key;
        table += "</th>";
    });

    table += "</tr></thead><tbody>";

    data.forEach(function(row) {

        table += "<tr>";

        Object.keys(row).forEach(function(key) {
            table += "<td>";
            table += row[key];
            table += "</td>";
        });

        table +="</tr>";

    });

    table += "</tbody></table></div>";
    return table;
}


function showToast(message){

    $(".toast").remove();

    var toastHtml =
        `<div class="toast" style="position:absolute;top:5%;right:5%" data-delay="3000">
                            <div class="toast-header">
                                <strong class="mr-auto text-primary">Notification</strong>
                                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
                            </div>
                            <div class="toast-body">
                                {{message}}
                            </div>
                        </div>`

    toastHtml = toastHtml.replace("{{message}}", message);

    $("html").append(toastHtml);
    $('.toast').toast('show');

}