<title>Simple Blog Application</title>
<!-- Required meta tags -->
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<!--Always first jQuery THEN Bootstrap-->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">


<script src="include/js/js.js"></script>
<script src="include/lib/jquery.editable.min.js"></script>

<link rel="stylesheet" href="include/css/style.css">

<style>
    @import url('https://fonts.googleapis.com/css?family=Aclonica|Anonymous+Pro:400,700|Archivo+Black|Calistoga|Carter+One|Chewy|Courgette|Fredoka+One|Frijole|Lalezar|Lato:400,900|Lilita+One|Patua+One|Paytone+One|Poppins:400,900|Roboto+Condensed|Roboto:400,700,900|Rubik+Mono+One|Sarabun:400,700,800|Satisfy&display=swap');
</style>