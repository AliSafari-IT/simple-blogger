<?php include "include/functions.php"; ?>

<html lang="en">

<head>

    <?php getHeader(); ?>

    <script>

        $(document).ready(function () {

            $.ajax({
                method: "post",
                url: 'include/api.php',
                data: {request: 'getUsers', data: ''}
            }).done(function (jsonData) {

                var data = JSON.parse(jsonData);
                console.log(data['data']);
                $(".table-users").html(generateTable(data['data']));


            });


        });

    </script>

</head>

<body>

<?php getNavigation(); ?>

<div class="container-fluid content">
    <div class="col-12">

        <h3>Users</h3>

        <div class="table-users"></div>

    </div>
</div>

<?php getFooter(); ?>

</body>

</html>