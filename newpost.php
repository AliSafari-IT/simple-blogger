<?php include "include/functions.php";
logincheck(); ?>

<html lang="en">

<head>

    <?php getHeader(); ?>

</head>

<body>

<?php getNavigation(); ?>

<div class="container content">
    <div class="col-12">

        <h3>Register</h3>

        <form data-request="newPost" data-url="include/api.php" data-method="POST">

            <div class="form-group">
                <label for="message">Message</label>
                <input data-data="message" type="text" class="form-control" id="message" placeholder="Message">
            </div>

            <button type="submit" class="btn btn-primary">Schrijven</button>

        </form>

    </div>
</div>

<?php getFooter(); ?>

</body>

</html>